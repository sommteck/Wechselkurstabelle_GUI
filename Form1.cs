using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Wechselkurstabelle_GUI
{
    public partial class Wechselkurstabelle : Form
    {
        public Wechselkurstabelle()
        {
            InitializeComponent();

            ToolTip help = new ToolTip();
            help.SetToolTip(cmd_view, "Umrechnen");
            help.SetToolTip(cmd_clear, "Ein- und Ausgaben löschen");
            help.SetToolTip(cmd_end, "Programm beenden");
            help.SetToolTip(txt_von, "Unterster Umrechnungswert eintragen");
            help.SetToolTip(txt_bis, "Oberster Umrechnungswert eintragen");
            help.SetToolTip(txt_kurs_dollar, "Wechselkurs Dollar");
            help.SetToolTip(txt_kurs_pfund, "Wechselkurs Pfund");
            help.SetToolTip(txt_kurs_yen, "Wechselkurs Yen");
            help.SetToolTip(dataGridView1, "Ausgabe");
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            txt_von.Text = txt_bis.Text = txt_kurs_dollar.Text = txt_kurs_pfund.Text = txt_kurs_yen.Text = null;
            txt_von.Focus();
        }

        private void cmd_view_Click(object sender, EventArgs e)
        {
            try
            {
                double von, bis, kurs_dollar, kurs_pfund, kurs_yen, dollar, pfund, yen;

                von = Convert.ToDouble(txt_von.Text);
                bis = Convert.ToDouble(txt_bis.Text);
                kurs_dollar = Convert.ToDouble(txt_kurs_dollar.Text);
                kurs_pfund = Convert.ToDouble(txt_kurs_pfund.Text);
                kurs_yen = Convert.ToDouble(txt_kurs_yen.Text);

                dataGridView1.Rows.Clear();

                for (; von <= bis; von = von + 1)
                {
                    dollar = von * kurs_dollar;
                    pfund = von * kurs_pfund;
                    yen = von * kurs_yen;

                    dataGridView1.Rows.Add(von.ToString("F2"), dollar.ToString("F2"), pfund.ToString("F2"), yen.ToString("F2"));
                }
            }

            catch (Exception)
            {
                MessageBox.Show("Bitte nur Zahlen eingeben!");
                txt_von.Focus();
            }
        }

    }
}
