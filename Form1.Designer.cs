namespace Wechselkurstabelle_GUI
{
    partial class Wechselkurstabelle
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txt_von = new System.Windows.Forms.TextBox();
            this.txt_bis = new System.Windows.Forms.TextBox();
            this.txt_kurs_dollar = new System.Windows.Forms.TextBox();
            this.txt_kurs_pfund = new System.Windows.Forms.TextBox();
            this.txt_kurs_yen = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.LN_Euro = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_Dollar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_Pfund = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LN_Yen = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmd_view = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "von €";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(30, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "bis €";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(175, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Kurs $";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(175, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Kurs £";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(175, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Kurs ¥";
            // 
            // txt_von
            // 
            this.txt_von.Location = new System.Drawing.Point(72, 28);
            this.txt_von.Name = "txt_von";
            this.txt_von.Size = new System.Drawing.Size(77, 22);
            this.txt_von.TabIndex = 5;
            // 
            // txt_bis
            // 
            this.txt_bis.Location = new System.Drawing.Point(72, 71);
            this.txt_bis.Name = "txt_bis";
            this.txt_bis.Size = new System.Drawing.Size(77, 22);
            this.txt_bis.TabIndex = 6;
            // 
            // txt_kurs_dollar
            // 
            this.txt_kurs_dollar.Location = new System.Drawing.Point(225, 28);
            this.txt_kurs_dollar.Name = "txt_kurs_dollar";
            this.txt_kurs_dollar.Size = new System.Drawing.Size(81, 22);
            this.txt_kurs_dollar.TabIndex = 7;
            // 
            // txt_kurs_pfund
            // 
            this.txt_kurs_pfund.Location = new System.Drawing.Point(225, 71);
            this.txt_kurs_pfund.Name = "txt_kurs_pfund";
            this.txt_kurs_pfund.Size = new System.Drawing.Size(81, 22);
            this.txt_kurs_pfund.TabIndex = 8;
            // 
            // txt_kurs_yen
            // 
            this.txt_kurs_yen.Location = new System.Drawing.Point(225, 109);
            this.txt_kurs_yen.Name = "txt_kurs_yen";
            this.txt_kurs_yen.Size = new System.Drawing.Size(81, 22);
            this.txt_kurs_yen.TabIndex = 9;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.LN_Euro,
            this.LN_Dollar,
            this.LN_Pfund,
            this.LN_Yen});
            this.dataGridView1.Location = new System.Drawing.Point(343, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(443, 237);
            this.dataGridView1.TabIndex = 10;
            // 
            // LN_Euro
            // 
            this.LN_Euro.HeaderText = "Euro";
            this.LN_Euro.Name = "LN_Euro";
            // 
            // LN_Dollar
            // 
            this.LN_Dollar.HeaderText = "Dollar";
            this.LN_Dollar.Name = "LN_Dollar";
            // 
            // LN_Pfund
            // 
            this.LN_Pfund.HeaderText = "Pfund";
            this.LN_Pfund.Name = "LN_Pfund";
            // 
            // LN_Yen
            // 
            this.LN_Yen.HeaderText = "Yen";
            this.LN_Yen.Name = "LN_Yen";
            // 
            // cmd_view
            // 
            this.cmd_view.Location = new System.Drawing.Point(33, 282);
            this.cmd_view.Name = "cmd_view";
            this.cmd_view.Size = new System.Drawing.Size(75, 32);
            this.cmd_view.TabIndex = 11;
            this.cmd_view.Text = "&Anzeigen";
            this.cmd_view.UseVisualStyleBackColor = true;
            this.cmd_view.Click += new System.EventHandler(this.cmd_view_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Location = new System.Drawing.Point(144, 282);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 32);
            this.cmd_clear.TabIndex = 12;
            this.cmd_clear.Text = "&Löschen";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.BackColor = System.Drawing.Color.Red;
            this.cmd_end.Location = new System.Drawing.Point(701, 282);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 32);
            this.cmd_end.TabIndex = 13;
            this.cmd_end.Text = "&Ende";
            this.cmd_end.UseVisualStyleBackColor = false;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // Wechselkurstabelle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 330);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_view);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.txt_kurs_yen);
            this.Controls.Add(this.txt_kurs_pfund);
            this.Controls.Add(this.txt_kurs_dollar);
            this.Controls.Add(this.txt_bis);
            this.Controls.Add(this.txt_von);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Wechselkurstabelle";
            this.Text = "Wechselkurstabelle";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txt_von;
        private System.Windows.Forms.TextBox txt_bis;
        private System.Windows.Forms.TextBox txt_kurs_dollar;
        private System.Windows.Forms.TextBox txt_kurs_pfund;
        private System.Windows.Forms.TextBox txt_kurs_yen;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Euro;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Dollar;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Pfund;
        private System.Windows.Forms.DataGridViewTextBoxColumn LN_Yen;
        private System.Windows.Forms.Button cmd_view;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_end;
    }
}

